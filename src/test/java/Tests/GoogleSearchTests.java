package Tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

public class GoogleSearchTests {

	public WebDriver driver;

	@BeforeMethod
	public void driverSetup() {
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeOptions option = new ChromeOptions();
		option.setHeadless(true);
		driver = new ChromeDriver(option);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void quitDriver() {
		driver.quit();
	}

	@Test
	public void performGoogleSearch() {

		driver.get("https://google.com");
		String ActualTitle = driver.getTitle();
		assertEquals("Google", ActualTitle);

		driver.findElement(By.xpath("//*[@name='q']")).sendKeys("Pact Testing" + Keys.RETURN);

		boolean eleDisplayed = driver.findElement(By.xpath("//*[@id='res']//span[text()='Pact testing']"))
				.isDisplayed();
		assertTrue(eleDisplayed);

		driver.quit();
	}
}
